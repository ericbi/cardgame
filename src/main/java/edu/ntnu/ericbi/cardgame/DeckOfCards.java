package edu.ntnu.ericbi.cardgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;


/**
 * A deck of cards. This class holds 52 cards upon initialization and has methods
 * for manipulating it, for example drawing cards.
 *
 * @author Eric Bieszczad-Stie
 * @version 2022-04-03
 */
public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> cards;

    /**
     * Creates a new instance of a deck of cards. This generates 52 playing cards that you would find
     * in a normal deck of cards. These are created in ascending order and in the following order: Spades, Hearts, Diamonds, Clubs.
     */
    public DeckOfCards() {
        cards = new ArrayList<>();
        // Loop for every suit
        for (int suitNumber = 0; suitNumber < 4; suitNumber++) {
            // Loop for every value
            for (int cardValue = 1; cardValue < 14; cardValue++) {
                cards.add(new PlayingCard(suit[suitNumber], cardValue));
            }
        }
    }

    /**
     * Get all the cards in the deck.
     *
     * @return an {@link ArrayList} of the playing cards in the deck.
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }

    /**
     * Returns n random cards from the card deck. The cards drawn from the deck are removed from the deck afterwards.
     *
     * @param n  The amount of cards to draw from the deck.
     * @return an ArrayList of cards.
     */
    public Collection<PlayingCard> dealHand(int n) {
        if (n > cards.size()) {
            throw new IllegalArgumentException("Not enough cards to deal the amount of cards requested");
        }

        Collection<PlayingCard> hand = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            hand.add(this.drawRandomCard());
        }

        return hand;
    }

    /**
     * Draw a random card from the deck. This card is removed from the deck after it is returned.
     *
     * @return a {@link PlayingCard}
     */
    private PlayingCard drawRandomCard() {
        if (cards.size() == 0) {
            throw new IllegalStateException("There are no cards in the deck and therefore a random card could not be retrieved.");
        }
        int i = ThreadLocalRandom.current().nextInt(cards.size());
        return cards.remove(i);
    }
}
