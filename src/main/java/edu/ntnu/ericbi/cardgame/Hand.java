package edu.ntnu.ericbi.cardgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A class representing a hand of cards. This class can be initialized with an empty hand. Imagine a "Hand"
 * as the cards that a player has (so it's a 1 to 1 relation to a player - each player has a hand of cards).
 *
 * @author Eric Bieszczad-Stie
 * @version 2022-04-03
 */
public class Hand {
    private Collection<PlayingCard> cards;

    /**
     * A default constructor for an empty hand.
     */
    public Hand() {
        cards = new ArrayList<>();
    }

    /**
     * Creates a new instance of a hand, given some starting cards.
     *
     * @param startingCards  Cards the hand should start with.
     */
    public Hand(PlayingCard... startingCards) {
        cards = new ArrayList<>(List.of(startingCards));
    }

    /**
     * Creates a new instance of a hand, given some starting cards.
     *
     * @param startingCards  A collection of cards the hand should start with.
     */
    public Hand(Collection<PlayingCard> startingCards) {
        cards = new ArrayList<>(startingCards);
    }

    /**
     * Get all cards in the hand.
     *
     * @return all cards in the hand.
     */
    public Collection<PlayingCard> getCards() {
        return cards;
    }

    /**
     * Check if the hand is a flush or not.
     *
     * @return true if there are exactly 5 cards and all of them are the same suit.
     */
    public boolean isFlush() {
        // A flush needs to be at least 5 cards.
        if (!(cards.size() == 5)) return false;

        // If there is only 1 suit in hand
        if (cards.stream().map(PlayingCard::getSuit).distinct().count() == 1) return true;

        // Otherwise it is not a flush.
        return false;
    }

    /**
     * Get the value of the hand. This is determined by adding the value of all cards in the hand.
     * The value of an Ace is 1 and the value of a king is 13. All other cards land between here.
     *
     * @return sum of all the card values in the hand.
     */
    public int getHandValue() {
        return cards.stream().map(PlayingCard::getFace).mapToInt(Integer::intValue).sum();
    }

    /**
     * Get all cards of suit heart.
     *
     * @return all cards from the hand that are of suit heart.
     */
    public Collection<PlayingCard> getHearts() {
        return cards.stream().filter(card -> card.getSuit() == 'H').toList();
    }

    /**
     * Check whether the hand contains a queen of spade.
     *
     * @return true if the hand has a queen of spades, false otherwise.
     */
    public boolean hasQueenOfSpades() {
        for (PlayingCard card : cards) {
            if (card.getSuit() == 'S' && card.getFace() == 12) return true;
        }
        return false;
    }
}
