package edu.ntnu.ericbi.cardgame;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * The controller class for the card game page. This is the main page of the card game and is where actions such as
 * dealing a hand and checking it, is possible.
 */
public class PageController {
    private static Hand currentHand = new Hand();

    /**
     * The text field that the result is written to.
     */
    @FXML
    private TextField cardsOfHeartResult;

    /**
     * The text field that the result is written to.
     */
    @FXML
    private TextField isFlushResult;

    /**
     * The text field that the result is written to.
     */
    @FXML
    private TextField queenOfSpadesResult;

    /**
     * The text field that the result is written to.
     */
    @FXML
    private TextField sumOfFacesResult;

    /**
     * The text element where the cards in the hand are shown.
     */
    @FXML
    private Text showCardsText;

    /**
     * Event when "Check Hand" button is pressed. This updates the sum of faces, queen of spades, is flush and
     * hearts cards text fields.
     *
     * @param event  The click event (Injected by JavaFX).
     */
    @FXML
    void checkHandBtnClicked(ActionEvent event) {
        // Set heart cards result
        String heartCards = String.join(" ", currentHand.getHearts().stream().map(PlayingCard::getAsString).toList());
        cardsOfHeartResult.setText(heartCards);

        // Set flush result
        if (currentHand.isFlush()) {
            isFlushResult.setText("Yes");
        } else {
            isFlushResult.setText("No");
        }

        // Set queen of spade result
        if (currentHand.hasQueenOfSpades()) {
            queenOfSpadesResult.setText("Yes");
        } else {
            queenOfSpadesResult.setText("No");
        }

        // Set sum of faces
        sumOfFacesResult.setText(String.valueOf(currentHand.getHandValue()));
    }

    /**
     * Even when "Deal Hand" button is clicked. This deals a new hand and updates the showCardsText element.
     * Note that as of now (2022-04-03), the cards a dealt from a new deck of cards each time. This is intentional
     * since the task does not specify anything around this, and using the same deck would end up in the deck
     * going empty after a while.
     *
     * @param event  The click event (Injected by JavaFX).
     */
    @FXML
    void dealHandBtnClicked(ActionEvent event) {
        // For now I'm just creating a new deck of cards every time. I could make this happen only
        // when I click a "reset" button, but for now the task doesn't specify this, so I'm keeping it simple.
        DeckOfCards cardDeck = new DeckOfCards();
        currentHand = new Hand(cardDeck.dealHand(5));

        showCardsText.setText(String.join(" ", currentHand.getCards().stream().map(PlayingCard::getAsString).toList()));
    }
}
