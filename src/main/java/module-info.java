module edu.ntnu.ericbi.cardgame {
    requires javafx.controls;
    requires javafx.fxml;


    opens edu.ntnu.ericbi.cardgame to javafx.fxml;
    exports edu.ntnu.ericbi.cardgame;
}