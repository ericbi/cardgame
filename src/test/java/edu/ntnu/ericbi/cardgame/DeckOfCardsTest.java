package edu.ntnu.ericbi.cardgame;


import junit.framework.TestCase;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest extends TestCase {
    public void testAmountOfCards() {
        DeckOfCards cardDeck = new DeckOfCards();
        assertEquals(52, cardDeck.getCards().size());
    }

    public void testDealHandRemovesCards() {
        DeckOfCards cardDeck = new DeckOfCards();

        Collection<PlayingCard> cards = cardDeck.dealHand(5);
        assertEquals(52 - 5, cardDeck.getCards().size());
    }

    public void testDealingMoreCardsThanInDeckThrowsException() {
        DeckOfCards cardDeck = new DeckOfCards();

        assertThrows(IllegalArgumentException.class, () -> cardDeck.dealHand(55));
    }
}