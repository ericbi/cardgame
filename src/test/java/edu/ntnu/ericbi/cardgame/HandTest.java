package edu.ntnu.ericbi.cardgame;

import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class HandTest extends TestCase {

    public void testEmptyConstructor() {
        new Hand();
    }

    public void testMultipleArgumentConstructor() {
        DeckOfCards cardDeck = new DeckOfCards();
        ArrayList<PlayingCard> c = (ArrayList<PlayingCard>) cardDeck.dealHand(3);
        new Hand(c.get(0), c.get(1), c.get(2));
    }

    public void testCollectionConstructor() {
        DeckOfCards cardDeck = new DeckOfCards();
        new Hand(cardDeck.dealHand(5));
    }

    public void testGetHearts() {
        DeckOfCards cardDeck = new DeckOfCards();
        Hand hand = new Hand(cardDeck.dealHand(15));

        int heartCounter = 0;
        for (PlayingCard card : hand.getCards()) {
            if (card.getSuit() == 'H') heartCounter++;
        }
        assertEquals(heartCounter, hand.getHearts().size());
    }
}